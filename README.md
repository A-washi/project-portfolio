# Project Portfolio

Albert Washington's Portfolio
------------------------------


# RevOverflow

## Project Description

RevOverflow is a site dedicated to providing support to technical problems encountered by the Revature community during training and project work. 
This platform is intended to provide a positive, constructive location to seek help from the Revature community regarding challenges that developers frequently face. It also helps facilitate transfer of knowledge between associates and batches, helps document and solve common problems, and provides a mechanism for associates to gain knowledge by helping one another. RevOverflow integrates with the Revature Swag Shop to provide the opportunity for participants to earn rewards.

## Technologies Used

* Spring 
* Java 
* SQL
* Hibernate 
* HTML 
* CSS 
* JavaScript 
* Log4J 
* JUnit 
* React
* Redux
* Agile-Scrum
* TypeScript 

## Features

List of features ready and TODOs for future development
* Allows users to post questions and answers. If their answers are confirmed, they recieve points.
* Users are able to find questions within various categories and filters.
* Administrators are able to add to a FAQ for users to view.

To-do list:
* Fix various bugs including ones left by predecessors.
* Enable a user to view other user profiles.

## Getting Started

   
This is a Spring Boot project meant to be run on a server. 
Run the backend project as a Spring Boot Application.
There is also a react app associated that needs to be installed and started.

Project is located here: https://github.com/875-Trevin-Chester

------------------------------------------------------------------------------



# Revature Social Network (React) v2

## Project Description

In Revature's Social Network everyone is friends with everyone else. Users can register, login to the application, and start sharing multimedia with everyone. Registered users are allowed to modify their personal information and upload their profile pictures.The application provides a search feature that allows users to search out friends and look at their profiles. Users are provided with a "feed", in which they can see what everyone is posting and like posts. Users can access and use the application via an interactive client-side single paged application that stores and retrieves multimedia using AWS S3 and consumes a RESTful web service that provides business logic and access to a database.

## Technologies Used

* Spring 
* Java 
* SQL
* Hibernate 
* HTML 
* CSS 
* JavaScript 
* Log4J 
* JUnit 
* React
* Redux
* Agile-Scrum

## Features

List of features ready and TODOs for future development
* Allows a user to create an account and create posts for others using the application to see.
* Users can add pictures to their posts when creating a post.
* Users can see and "like" a post that the see in the the feed of all posts. 
* Users are able to change and update their own information including their profile picture, username, and password.  

To-do list:
* Enable post deletion.
* Allow Youtube to be embedded in a post.
* Tweaks to the UI and color scheme.

## Getting Started

   
This is a .war Maven project meant to be run on a server, namely Apache Tomcat. 
Add the project to a server and run the server.
There is also a react app associated that needs to be installed and started.


------------------------------------------------------------------------------




# Expense Reimbursement System (ERS)

## Project Description

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. 
All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. 
Finance managers can log in and view all reimbursement requests and past history for all employees in the company. 
Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used

* Servlets 
* Java 8
* JavaScript 
* HTML 
* CSS 
* JDBC

## Features

List of features ready and TODOs for future development
* Allows a user to create reimbursement requests with details like description and the type of reimbursement. 
* As a manager, you are able to approve or deny requests from users.
* Sort reimbursements by date, submitter, or status or filter by the status as well.  

To-do list:
* Allow for user account creation.
* Prevent a reimbursement from being denied or approved more than once.
* Allow for details on why a reimbursement was denied to be added.

## Getting Started

A database will need to be created and the "Project1_Creation_Script.sql will need to ran in that database.
   
This is a .war Maven project meant to be run on a server, namely Apache Tomcat. 
Add the project to a server and run the server.

The endpoint, "/ExpenseReimbursement" will go to the login page.
The SQL script can be edited to add users or initial reimbursements otherwise the main two test accounts are:

* Employee
Username: test
Password: test

* Finance Manager
Username: testMan
Password: test

