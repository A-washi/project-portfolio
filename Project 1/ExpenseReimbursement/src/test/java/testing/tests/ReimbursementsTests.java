package testing.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

import models.Reimbursement;
import models.User;
import models.dao.ReimbursementDAO;
import models.dao.ReimbursementDAOImpl;
import models.service.ReimbursementService;
import models.service.ReimbursementServiceImpl;


public class ReimbursementsTests 
{
	private static ReimbursementDAO rdao;
	private static ReimbursementService rser;
	private static User testUser;
	private static User testManager;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		 rdao = new ReimbursementDAOImpl("jdbc:h2:./src/test/resources/ReimbursementsTestDB", "aw", "aw");
		 rser = new ReimbursementServiceImpl(rdao);
		 testUser = new User(88, "testEmployee", "Te", "blbhabh@deme.com", 1, "Testkun", "Employee'O Connell");
		 testManager = new User(99, "testManager", "Tm", "blbha33bh@deme.com", 2, "Manachan", "Managerwell");
		 Reimbursement testReimb = new Reimbursement(435.23, testUser, 1, 4); 
		 
		 rdao.insertReimbursement(testReimb);
		
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception 
	{
		rdao.h2InitDao();
	}

	@After
	public void tearDown() throws Exception 
	{
		rdao.h2DestroyDao();
	}

	@Test
	public void insertReimbDaoTest()
	{
		Reimbursement testReimb3 = new Reimbursement(45.22, testUser, 1, 3);
		boolean testBool = rdao.insertReimbursement(testReimb3);
		assertEquals("Successfully insert reimbursement", true, testBool);
	}
	
	@Test
	public void getReimbDaoTest() 
	{
		//Dealing with select all
		List<Reimbursement> rlist = rdao.selectAllReimbursements();
		
		assertNotEquals("The list of reimbursements is not null.", null, rlist);
		assertEquals("Test Reimbursement is properly found.", 5, rlist.get(0).getId());
		
		//Dealing with select by author
		rlist = rdao.selectAllReimbursementsByAuthor(testUser);
		
		assertEquals("Only one reimbursement for test user was found", 1, rlist.size());
		assertEquals("test User's reimbursement was found.", 88 , rlist.get(0).getAuthor().getUserId());
		assertEquals("Got the correct $ amount", 435.23, rlist.get(0).getAmount());
		
		//Dealing with reimbursement model
		assertEquals("The status name was properly set to Pending", "Pending", rlist.get(0).getStatusName());
		assertEquals("The Type name was properly set to Other per creation", "Other", rlist.get(0).getTypeName());
		
	}
	
	@Test
	public void getReimbServiceTest()
	{
		//Reimbursement without setting the status. Status should default to 1 = pending.
		Reimbursement testReimb2 = new Reimbursement(0.32, testUser, 2);
		
		assertEquals("Can successfully add reimbursement.", true, rser.addNewReimbursement(testReimb2));
		assertNotEquals("Employee (role ID 1) cannot approve a reimbursement.", true, rser.approveReimbursement(testReimb2.getId(), testUser ));
		assertNotEquals("Employee (role ID 1) cannot deny a reimbursement.", true, rser.denyReimbursement(testReimb2.getId(), testUser));
		

		

	}
	
	@Test
	public void approvalAndDenialTest()
	{
		Reimbursement testReimb2 = new Reimbursement(0.32, testUser, 2);
		rser.addNewReimbursement(testReimb2);
		testManager.setRoleId(2);
			
		assertEquals("Can successfully approve reimbursment.", true, rser.approveReimbursement(testReimb2.getId(), testManager));
		
		
		//In the future, intend to add logic to service to prevent overwriting resolutions. Currently, prohibits it on the client's side.
		assertEquals("Can successfully deny reimbursment.", true, rser.denyReimbursement(testReimb2.getId(), testManager));
	}

}
