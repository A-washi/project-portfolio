package testing.programs;

import java.util.List;

import models.Reimbursement;
import models.User;
import models.dao.ReimbursementDAO;
import models.dao.ReimbursementDAOImpl;
import models.dao.UsersDAO;
import models.dao.UsersDAOImpl;
import models.service.ReimbursementService;
import models.service.ReimbursementServiceImpl;
import models.service.UsersService;
import models.service.UsersServiceImpl;

public class TestProgram 
{

	public static void main (String[] args)
	{
		UsersDAO uD = new UsersDAOImpl();
		UsersService uServ = new UsersServiceImpl(); 
		
		List<User> nl = uD.selectAllUsers();
		
		for (User u : nl)
		{
			System.out.println(u);
		}
		
		User logUser = uServ.login("BColl", "Crux1s");
		
		System.out.println(logUser);
		
		
		ReimbursementDAO rD = new ReimbursementDAOImpl();
		ReimbursementService rs = new ReimbursementServiceImpl();
		
		//List <Reimbursement> rlistfortest = rs.getAllReimbursementsbyAuthor(new User(4, "test", "test", "", 1, "", ""));
		List <Reimbursement> rlistfortest = rs.getAllReimbursements();
		
		for(Reimbursement r : rlistfortest)
		{
			System.out.println();
			System.out.println("ID: " + r.getId());
			System.out.println(r.getAmount());
			System.out.println(r.getSubmitDate());
			System.out.println(r.getDescription());
			System.out.println(r.getAuthor().getUsername());
			System.out.println(r.getStatusID());
			System.out.println("Status: " + r.getStatusName());
			System.out.println(r.getTypeID());
			System.out.println("type: " + r.getTypeName());
			System.out.println("resolved date: " + r.getResolutionDate());
			
			try
			{
				System.out.println("resolver: " + r.getResolver().getUsername());
			}
			catch (NullPointerException e)
			{
				System.out.println("resolver: N/A");
			}
			System.out.println();
		}
	}
}
