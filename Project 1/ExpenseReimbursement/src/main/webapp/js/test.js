let tarry = 
    [{"id" : 3, "name" : "red", "season": "fall", "state" : true}
    , {"id" : 2, "name" : "blue", "season": "spring", "state" : false}
    , {"id" : 5, "name" : "green", "season": "summer", "state" : true}];


// console.log(tarry.sort((a,b) => a.id - b.id));

// tarry.forEach(element => {
//     console.log(element);
    
// });

console.log(tarry.sort((a,b) => {
    let fa = a.name.toLowerCase(),
        fb = b.name.toLowerCase();

    if (fa < fb) {
        return -1;
    }
    if (fa > fb) {
        return 1;
    }
    return 0;
}));

tarry.forEach(element => 
{
    console.log(element);
    
});

let number = 56;
let string = "56";

console.log(number == string);
console.log(number === string);

//I don't mind, if you have time. I know everyone is crunching so I don't want to be in anyone's way.