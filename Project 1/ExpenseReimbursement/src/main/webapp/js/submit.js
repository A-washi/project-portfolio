
document.getElementById("submission-form").addEventListener("submit", submitReimbursement);

window.onload = function () {

    fetch('http://localhost:9003/ExpenseReimbursement/AJAX/CurrentUname', { method: "post" })
        .then(function (resp) {
            const convertedResponse = resp.json();
            return convertedResponse;
        })
        .then(function (cResponse) {
            console.log(cResponse);
            document.getElementById("author-submit").value = cResponse.data[0];
        })
};


function stringifyForm(form) {
    // Turn all inputs in the form into an array of URL-encoded key/value pairs.
    let urlEncodedDataPairs = [];
    for(input of form.getElementsByTagName('input'))
        urlEncodedDataPairs.push(encodeURIComponent(input.name) + '=' + encodeURIComponent(input.value));
    
    for(input of form.getElementsByTagName('textarea'))
        urlEncodedDataPairs.push(encodeURIComponent(input.name) + '=' + encodeURIComponent(input.value));

    for(input of form.getElementsByTagName('select'))
        urlEncodedDataPairs.push(encodeURIComponent(input.name) + '=' + encodeURIComponent(input.value));

        
    // Combine the pairs into a single string and replace all %-encoded spaces to
    // the '+' character; matches the behavior of browser form submissions.
    return urlEncodedDataPairs.join('&').replace(/%20/g, '+');
};




function submitReimbursement(event1) {


    event1.preventDefault();
    console.log(event1);
    console.log(event1.currentTarget); //gives the element that has the event listener
    console.log(stringifyForm(event1.currentTarget));
    fetch('http://localhost:9003/ExpenseReimbursement/AJAX/submitReimbursement',
        {
            headers:
            {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            method: "post",
            body: stringifyForm(event1.currentTarget)
        })
        .then(function (resp) {

            return resp.json();
        })
        .then(function(json)
        {
            let succMess = document.getElementById("submit-success-message");
            succMess.hidden = false;
            console.log(json);

            if (json == true) 
            {
                succMess.innerText = "Reimbursement successfully submitted.";
            }
            else 
            {
                succMess.innerText = "Reimbursement unsuccessfully submitted.";
            }

            document.getElementById("amount-submit").value = '';
            document.getElementById("description-submit").value = '';            

            setTimeout(function () {
                succMess.hidden = true;

            }, 10000)
        })
};

function backToViewReimbursements() 
{
    location.replace("http://localhost:9003/ExpenseReimbursement/reimbursements/reimbursement-employee")
}