console.log("In reimbursement page js");
let reimbursements;
let userRole;
let selectedReimbursement;

window.onload = function()
    {

        

        fetch('http://localhost:9003/ExpenseReimbursement/AJAX/CurrentUname', {method:"post"})
            .then(function(resp) 
            {
				const convertedResponse = resp.json();
				return convertedResponse;
            })
            .then(function(cResponse) 
            {
                console.log(cResponse);
                document.getElementById("user-name").innerText = cResponse.data[0];
                userRole = cResponse.data[1];
                console.log("Role: " + userRole);

                if (userRole == 2) 
                {
                    revealManagerElements();
                }
                
                
            });

        getReimbursements();
            
    };


    function getReimbursements()
    {
        fetch('http://localhost:9003/ExpenseReimbursement/AJAX/UserInformation', {method:"post"})
        .then(function(resp) 
        {
            const convertedResponse2 = resp.json();
            return convertedResponse2;
        })
        .then(function(cResponse2) 
        {
            console.log(cResponse2);
            if (cResponse2.rList == null) 
            {
                document.getElementById("reimbursement-view").innerText = "There are no reimbursements."         
            }
            else
            {
                reimbursements = cResponse2.rList;

                if(userRole == 1)
                {
                    makeReimbursementTableEmployee(reimbursements);
                }
                else if(userRole == 2)
                {
                    
                    makeReimbursementTableFinManager(reimbursements)
                }
                else
                {
                    console.log("The current user role is not implemented.");
                    document.getElementById("reimbursement-view").innerText = "There are no reimbursements."         

                }

                console.log(reimbursements[0]);


            }
        })

    };

    function makeReimbursementTableEmployee(rList)
    {
        console.log("Making Employee table.");

        let newTable = document.createElement("table");
        newTable.classList.add( "table", "table-hover");
        let tableHeaders = document.createElement("tr");

        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("ID"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Amount"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Date Submitted"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Description"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Type"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Status"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Resolution Date"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Resolved By"));

        
        newTable.appendChild(tableHeaders);

        for (let i = 0; i < rList.length; i++)
        {
            let nxtRow = document.createElement("tr");
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].id)); 
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].amount)); 
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].submitDate)); 
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].description)); 
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].typeName));
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].statusName));
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].resolutionDate));

            if(rList[i].resolver)
            {
                nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].resolver.firstName + " " + rList[i].resolver.lastName)); 
            }
            else
            {
                nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode("")); 
            }

            nxtRow.classList.add("reimbursement-table-row");
            nxtRow.id = "reimb-" + rList[i].id;
            
            newTable.appendChild(nxtRow);
        };

        let rView = document.getElementById("reimbursement-view");
        rView.innerHTML = "";
        rView.appendChild(newTable);


    };

    function makeReimbursementTableFinManager(rList)
    {
        console.log("Making manager table.");

        let newTable = document.createElement("table");
        newTable.classList.add( "table", "table-hover");
        let tableHeaders = document.createElement("tr");

        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("ID"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Amount"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Submitted By"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Date Submitted"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Description"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Type"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Status"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Resolution Date"));
        tableHeaders.appendChild(document.createElement("th")).appendChild(document.createTextNode("Resolved By"));

        
        newTable.appendChild(tableHeaders);

        for (let i = 0; i < rList.length; i++)
        {
            let nxtRow = document.createElement("tr");
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].id)); 
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].amount));
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].author.firstName + " " + rList[i].author.lastName));  
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].submitDate)); 
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].description)); 
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].typeName));
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].statusName));
            nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].resolutionDate));

            if(rList[i].resolver)
            {
                nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode(rList[i].resolver.firstName + " " + rList[i].resolver.lastName)); 
            }
            else
            {
                nxtRow.appendChild(document.createElement("td")).appendChild(document.createTextNode("")); 
                
            }

            nxtRow.classList.add("reimbursement-table-row");
            nxtRow.id = "reimb-" + rList[i].id;
            nxtRow.addEventListener("click", function(event)
            {
                statusChange(event);
            });
            
            newTable.appendChild(nxtRow);

        }


        let rView = document.getElementById("reimbursement-view");
        rView.innerHTML = "";
        rView.appendChild(newTable);

    }

    function sortReimbursements()
    {
        console.log("In event function");
        let dateRadio = document.getElementById("submitDate-switch");
        let statusRadio = document.getElementById("status-switch");
        let authorRadio = document.getElementById("submitter-switch");
        if(reimbursements)
        {
            
            if (document.getElementById("submitDate-switch").checked) 
            {
                reimbursements.sort((a,b) => {
                    let fa = a.submitDate.toLowerCase();
                        fb = b.submitDate.toLowerCase();
                
                    if (fa < fb) {
                        return -1;
                    }
                    if (fa > fb) {
                        return 1;
                    }
                    return 0;
                });
            }
            else if(document.getElementById("status-switch").checked)
            {
                console.log("should be switch sort");
                reimbursements.sort((a,b) => {
                    let fa = a.statusName.toLowerCase();
                    let fb = b.statusName.toLowerCase();
                
                    if (fa < fb) {
                        return -1;
                    }
                    if (fa > fb) {
                        return 1;
                    }
                    return 0;
                });
            }
            else if(document.getElementById("submitter-switch").checked)
            {
                reimbursements.sort((a,b) => {
                    let fa = a.author.lastName.toLowerCase();
                    let fb = b.author.lastName.toLowerCase();
                
                    if (fa < fb) {
                        return -1;
                    }
                    if (fa > fb) {
                        return 1;
                    }
                    return 0;
                });
            }

            if(userRole == 1)
            {
                makeReimbursementTableEmployee(reimbursements);
            }
            else if(userRole == 2)
            {
                
                makeReimbursementTableFinManager(reimbursements);
            }


        }
        else
        {
            console.log("No reimbursements to sort.");
        }
    }

    function filterStatus(event)
    {

        let filterReimbursements = reimbursements;

        if (event.currentTarget.id == "filter-btn-pending") 
        {
            filterReimbursements = filterReimbursements.filter((r) => r.statusID == 1);
        }
        else if (event.currentTarget.id == "filter-btn-approved")
        {
            
            filterReimbursements = filterReimbursements.filter((r) => r.statusID == 2);
        }
        else if (event.currentTarget.id == "filter-btn-denied")
        {
            filterReimbursements = filterReimbursements.filter((r) => r.statusID == 3);
        }
        else if(event.currentTarget.id == "filter-btn-all")
        {
            
        }


        if (filterReimbursements) 
        {
            if(userRole == 1)
            {
                makeReimbursementTableEmployee(filterReimbursements);
            }
            else if(userRole == 2)
            {
                
                makeReimbursementTableFinManager(filterReimbursements);
            }

            
        }
        else
        {
            document.getElementById("reimbursement-view").innerHTML = "There are no reimbursements to show."
        }
    }

    function statusChange(event)
    {
        if (userRole == 1) 
        {
            console.log("User does not have permission to change statuses.");
            document.getElementById("reimb-stats").hidden = true;
        }
        else if (userRole == 2)
        {
            document.getElementById("reimb-stats").hidden = false;
            let eID = event.currentTarget.id;
            eID = eID.substring(6);
    
            let findRes = reimbursements.find(ele => 
                {
                    return ele.id == eID;
                });    
            
            console.log(findRes);
            document.querySelector("#amSubmitter").innerText = "Submitter: " + findRes.author.firstName + " " + findRes.author.lastName;
            document.querySelector("#amId").innerText = "ID: " + findRes.id;
            document.querySelector("#amAmt").innerText = "Amount: " + findRes.amount;
            document.querySelector("#amType").innerText = "Type Name: " + findRes.typeName;
            document.querySelector("#amStatus").innerText = "Status Name: " + findRes.statusName;
            document.querySelector("#amSubDate").innerText = "Submission Date: " + findRes.submitDate;
            document.querySelector("#amResDate").innerText = "Resolution Date: " + findRes.resolutionDate;
            document.querySelector("#amDesc").innerText = "Description: " + findRes.description;
            if(findRes.resolver)
            {
                document.querySelector("#amResolver").innerText = "Resolver: " + findRes.resolver.firstName + " " + findRes.resolver.lastName;
            }
            else
            {
                document.querySelector("#amResolver").innerText = "Resolver: ";
            }
            
            selectedReimbursement = findRes;
        }
    }

    function revealManagerElements()
    {
        document.getElementById("submitter-switch").hidden = false;
        document.getElementById("submitter-switch-label").hidden = false;
        document.getElementById("insert-page").hidden = true;
        document.getElementById("reimb-stats").hidden = true;

    }

    function approveReimbursement()
    {
        if (selectedReimbursement && selectedReimbursement.resolver == null) 
        {
            console.log(JSON.stringify(selectedReimbursement));

            fetch('http://localhost:9003/ExpenseReimbursement/AJAX/approveReimbursement',
                    {
                        method: 'post',
                        'headers': {
                            'Content-Type': 'application/json'
                        },
                        'body': selectedReimbursement.id
                        
                    })
                    .then(
                        function(resp)
                        {
                            const convertedResponse = resp.json();
                            return convertedResponse;
                        })
                    .then(
                        function(success)
                        {
                            document.getElementById("approval-msg").hidden = false;
                            if (success === true || success == "true") 
                            {   
                                document.getElementById("status-change-msg").innerText = "reimbursement id: " + selectedReimbursement.id + " successfully approved.";
                            }
                            else
                            {
                                document.getElementById("status-change-msg").innerText = "reimbursement id: " + selectedReimbursement.id + " could not be approved. Check role permissions or contact administrator.";
                            }

                            selectedReimbursement = null;
                        })
                
        }
        else
        {
            getElementById("status-change-msg").innerText = "Could not approve reimbursement. The reimbursement may already be resolved.";
            selectedReimbursement = null;
        }
        

        document.getElementById("reimb-stats").hidden = true;
        getReimbursements();
    }

    function denyReimbursement()
    {
        if (selectedReimbursement && selectedReimbursement.resolver == null) 
        {
            console.log(JSON.stringify(selectedReimbursement));

            fetch('http://localhost:9003/ExpenseReimbursement/AJAX/denyReimbursement',
                    {
                        method: 'post',
                        'headers': {
                            'Content-Type': 'application/json'
                        },
                        'body': selectedReimbursement.id
                        
                    })
                    .then(
                        function(resp)
                        {
                            const convertedResponse = resp.json();
                            return convertedResponse;
                        })
                    .then(
                        function(success)
                        {
                            document.getElementById("approval-msg").hidden = false;
                            if (success === true || success == "true") 
                            {   
                                document.getElementById("status-change-msg").innerText = "reimbursement id: " + selectedReimbursement.id + " successfully denied.";
                            }
                            else
                            {
                                document.getElementById("status-change-msg").innerText = "reimbursement id: " + selectedReimbursement.id + " could not be denied. Check role permissions or contact administrator.";
                            }

                            selectedReimbursement = null;
                        })
                
        }
        else
        {
            getElementById("status-change-msg").innerText = "Could not deny reimbursement. The reimbursement may already be resolved.";
            selectedReimbursement = null;
        }
        

        document.getElementById("reimb-stats").hidden = true;
        getReimbursements();
    }


    

    function insertPage() 
    {
        location.replace("http://localhost:9003/ExpenseReimbursement/reimbursements/reimbursement-submission")
    }

    function logOut()
    {
        location.replace("http://localhost:9003/ExpenseReimbursement/")
    }

