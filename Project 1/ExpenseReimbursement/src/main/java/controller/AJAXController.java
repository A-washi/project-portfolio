package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.Reimbursement;
import models.User;
import models.service.ReimbursementService;
import models.service.ReimbursementServiceImpl;
import utility.helpers.ReimbursementHelper;

public class AJAXController 
{
	final static Logger loggy = Logger.getLogger(AJAXController.class);

	
	public static void sendUsername(HttpServletRequest req, HttpServletResponse res) throws IOException, HTTPException
	{
		
		
		PrintWriter printer = res.getWriter();
		if(!req.getMethod().equals("POST")) 
		{
			
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "Bad method", null)));
			return;

			
		}
		
		User currentUser = (User)req.getSession().getAttribute("loggeduser");
		
		if(currentUser == null)
		{
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "User is not logged in.", null)));
			return;

		}
		
		
		printer.write(new ObjectMapper().writeValueAsString(new RequestResult(true, "Success", new String[] {currentUser.getUsername(), Integer.toString(currentUser.getRoleId())})));
	
		
	}

	public static void getUserReimbursements(HttpServletRequest req, HttpServletResponse res) throws IOException, HTTPException
	{
		System.out.println("Getting reimbursements");
		
		PrintWriter printer = res.getWriter();
		if(!req.getMethod().equals("POST")) 
		{
			
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "Bad method", null)));
			return;

			
		}
		
		User currentUser = (User)req.getSession().getAttribute("loggeduser");
		
		if(currentUser == null)
		{
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "User is not logged in.", null)));
			return;

		}
		
		ReimbursementService rServ = new ReimbursementServiceImpl();
		
		List<Reimbursement> uReimbursements = new ArrayList<Reimbursement>(); 
		
		
		//try catch this.
		if(currentUser.getRoleId() == 1)
		{
			uReimbursements = rServ.getAllReimbursementsbyAuthor(currentUser);
		}
		if(currentUser.getRoleId() == 2)
		{
			uReimbursements = rServ.getAllReimbursements();
		}
		
		
		if(uReimbursements == null || uReimbursements.isEmpty())
		{
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "There are no reimbursements to this user.", null)));
			return;

		}
		
		//Clear passwords before being sent.
		for (Reimbursement r : uReimbursements)
		{
			r.getAuthor().setPassword("");
			if(r.getResolver() != null)
			{
				r.getResolver().setPassword("");
			}
			
		}
		
		RequestResult rr = new RequestResult(true, "Success. Sent reimbursement list.", null);
		rr.setrList(uReimbursements);

		printer.write(new ObjectMapper().writeValueAsString(rr));
	
	}
	
	public static void submitEmployeeReimbursement(HttpServletRequest req, HttpServletResponse res) throws IOException, HTTPException
	{
		if(!req.getMethod().equals("POST")) 
		{
			PrintWriter printer = res.getWriter();
			printer.println("<html><body><h1>Don't hack me bro!</h1></body></html>");
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "Unauthorized Http method.", null)));
			return;
		}
		
		User loggedUser = (User)req.getSession().getAttribute("loggeduser");
		
		
		//check to see if there is a user in the session
		if(loggedUser == null) 
		{
			loggy.warn("No user in session.");
			System.out.println("No user logged in.");
			PrintWriter printer = res.getWriter();
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "No user logged in.", null)));
			return;

		}
		
		

		System.out.println("Extracting info.");
		//extracting the form data
		String tempAmount = req.getParameter("amount");
		String tempType = req.getParameter("type-choice");
		String description = req.getParameter("description");
		double amount;
		int type;
		
		ReimbursementService rServ = new ReimbursementServiceImpl();
		
		
		//convert form variables
		try
		{
			ReimbursementHelper rHelper = new ReimbursementHelper();
			amount = Double.parseDouble(tempAmount);
			type = rHelper.typeConversion(tempType);
			
		}
		catch(NumberFormatException e)
		{
			loggy.warn("Unable to parse amount in reimbursement submission for " + loggedUser, e);
			System.out.println("number exception.");
			PrintWriter printer = res.getWriter();
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "Parsing failure.", null)));
			return;
		}
		
		//Add description
		Reimbursement newReimb = new Reimbursement(amount, loggedUser, 1, type);
		newReimb.setDescription(description);
		
		boolean insertSuccess = rServ.addNewReimbursement(newReimb);
		System.out.println(insertSuccess);
		
		

		PrintWriter printer = res.getWriter();
		printer.write(new ObjectMapper().writeValueAsString(insertSuccess));

		//return "/html/reimbursement-employee.html";
		
	}

	public static void approveReimbursement(HttpServletRequest req, HttpServletResponse res) throws IOException, HTTPException
	{
		User currentUser = (User)req.getSession().getAttribute("loggeduser");
		boolean approveSuccess = false;
		PrintWriter printer = res.getWriter();
		
		if(!legitmateRequest(req, res))
		{
			loggy.warn("A non-legitimate request was made.");
			System.out.println("Not a legitimate request.");
			printer.write(new ObjectMapper().writeValueAsString(approveSuccess));
		}
		else
		{
//			ObjectMapper mapper = new ObjectMapper();
//	        Reimbursement reimb = mapper.readValue(req.getInputStream(), Reimbursement.class); 
			//System.out.println(new ObjectMapper().readValue(req.getInputStream(), Integer.class));
			
			//String rID = req.getParameter("id");
			
			
			try
			{
				int reimbID = new ObjectMapper().readValue(req.getInputStream(), Integer.class);
				System.out.println(reimbID);
//				reimbID = Integer.parseInt(rID);
				ReimbursementService rServ = new ReimbursementServiceImpl();
				approveSuccess = rServ.approveReimbursement(reimbID, currentUser);
				
				printer.write(new ObjectMapper().writeValueAsString(approveSuccess));
			}
			catch(NumberFormatException e)
			{
				loggy.warn("Error in parsing ID to int.", e);
				System.out.println("Error in parsing ID to int.");
				printer.write(new ObjectMapper().writeValueAsString(approveSuccess));
			}
			catch(Exception e)
			{
				loggy.warn("Error in manipulating request output.", e);
				System.out.println("Error in manipulating request output.");
				e.printStackTrace();
				printer.write(new ObjectMapper().writeValueAsString(approveSuccess));
			}
		}
		
		
	}
	
	public static void denyReimbursement(HttpServletRequest req, HttpServletResponse res) throws IOException, HTTPException
	{
		User currentUser = (User)req.getSession().getAttribute("loggeduser");
		boolean denySuccess = false;
		PrintWriter printer = res.getWriter();
		
		if(!legitmateRequest(req, res))
		{
			loggy.warn("A non-legitimate request was made.");
			System.out.println("Not a legitimate request.");
			printer.write(new ObjectMapper().writeValueAsString(denySuccess));
		}
		else
		{	
			
			try
			{
				int reimbID = new ObjectMapper().readValue(req.getInputStream(), Integer.class);
				System.out.println(reimbID);
				ReimbursementService rServ = new ReimbursementServiceImpl();
				denySuccess = rServ.denyReimbursement(reimbID, currentUser);
				
				printer.write(new ObjectMapper().writeValueAsString(denySuccess));
			}
			catch(NumberFormatException e)
			{
				loggy.warn("Error in parsing ID to int.", e);
				System.out.println("Error in parsing ID to int.");
				printer.write(new ObjectMapper().writeValueAsString(denySuccess));
			}
			catch(Exception e)
			{
				loggy.warn("Error in manipulating request output.", e);
				System.out.println("Error in manipulating request output.");
				e.printStackTrace();
				printer.write(new ObjectMapper().writeValueAsString(denySuccess));
			}
		}

		
	}
	
	private static boolean legitmateRequest (HttpServletRequest req, HttpServletResponse res) throws IOException, HTTPException
	{
		PrintWriter printer = res.getWriter();
		if(!req.getMethod().equals("POST")) 
		{
			
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "Bad method", null)));
			return false;

			
		}
		
		User currentUser = (User)req.getSession().getAttribute("loggeduser");
		
		if(currentUser == null)
		{
			printer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "User is not logged in.", null)));
			return false;

		}
		
		return true;
		
	}

	
}
