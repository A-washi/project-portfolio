package controller;

import java.util.List;

import models.Reimbursement;

public class RequestResult 
{
	private boolean success;
	private String message;
	private Object[] data;
	private List<Reimbursement> rList; 
	
	
	
	public RequestResult() 
	{
		
	}
	
	public RequestResult(boolean success, String message, Object[] data) 
	{
		this.success = success;
		this.message = message;
		this.data = data;
	}
	
	public boolean isSuccess() 
	{
		return success;
	}
	public void setSuccess(boolean success) 
	{
		this.success = success;
	}
	public String getMessage() 
	{
		return message;
	}
	public void setMessage(String message) 
	{
		this.message = message;
	}
	public Object[] getData() 
	{
		return data;
	}
	public void setData(Object[] data) 
	{
		this.data = data;
	}

	public List<Reimbursement> getrList() {
		return rList;
	}

	public void setrList(List<Reimbursement> rList) {
		this.rList = rList;
	}


	
	
}
