package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import models.Reimbursement;
import models.User;
import models.service.ReimbursementService;
import models.service.ReimbursementServiceImpl;
import models.service.UsersService;
import models.service.UsersServiceImpl;
import servlets.AJAXRequestHelper;

public class LoginController
{
	final static Logger loggy = Logger.getLogger(AJAXRequestHelper.class);

	public static String login(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException
	{


		if(!req.getMethod().equals("POST")) 
		{
			PrintWriter printer = res.getWriter();
			printer.println("<html><body><h1>Don't hack me bro!</h1></body></html>");

			return "/html/failed-login.html";
		}

		//extracting the form data
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		UsersService uServ = new UsersServiceImpl();
		ReimbursementService rServ = new ReimbursementServiceImpl();
		
		//check login
		User loggedUser = uServ.login(username, password);
		

		//check to see if the user has the correct username and password
		if(loggedUser == null) 
		{
			loggy.warn("Failed login for " + username);
			System.out.println("Failed login");
			return "/html/failed-login.html";
		}
		else 
		{

			loggy.info(loggedUser + " successfully logged in.");
			System.out.println(loggedUser);

			req.getSession().setAttribute("loggeduser", loggedUser);
			
			
			return "/reimbursements/reimbursement-employee";
		}
	}
}

