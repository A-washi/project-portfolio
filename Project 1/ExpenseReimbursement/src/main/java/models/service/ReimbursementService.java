package models.service;

import java.util.List;

import models.Reimbursement;
import models.User;

public interface ReimbursementService 
{
	public List<Reimbursement> getAllReimbursements();
	
	public List<Reimbursement> getAllReimbursementsbyAuthor(User author);
	
	public List<Reimbursement> getAllReimbursementsbyResolver(User resover);
	
	public List<Reimbursement> getAllReimbursementsByStatus(int statusID);
	
	public boolean addNewReimbursement(Reimbursement reimb);
	
	public boolean resolveReimbursement(Reimbursement reimb, User resolver);
	
	public boolean approveReimbursement(Reimbursement reimb, User resolver);
	
	public boolean denyReimbursement(Reimbursement reimb, User resolver);
	
	public boolean setReimbursementToPending(Reimbursement reimb, User resolver);
	
	public boolean removeReimbursement(Reimbursement reimb, User resolver);
	
	public void displayReimbursement(Reimbursement reimb);

	public boolean approveReimbursement(int reimbID, User resolver);

	public boolean denyReimbursement(int reimbID, User resolver);
	
	
}
