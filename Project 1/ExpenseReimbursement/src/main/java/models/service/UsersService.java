package models.service;

import models.User;

public interface UsersService 
{
	public User login(String userN, String pass) ;
	
	public int checkUserRole(User u);
}
