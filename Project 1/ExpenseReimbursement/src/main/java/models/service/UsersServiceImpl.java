package models.service;

import org.apache.log4j.Logger;

import models.User;
import models.dao.UsersDAO;
import models.dao.UsersDAOImpl;

public class UsersServiceImpl implements UsersService 
{
	final static Logger loggy = Logger.getLogger(UsersServiceImpl.class);
	
	public UsersDAO uDAO = new UsersDAOImpl();

	/**
	 * Compares the login credentials with those in the system. If they match return true. <br>
	 * 
	 * @returns boolean true if the specified username and password match a record in the DB.
	 * @param String userN Specified username.
	 * @param String pass Specified password.
	 * @Override
	 */
	public User login(String userN, String pass) 
	{
		User selectUser = uDAO.selectUserbyUsername(userN);
		
		if(selectUser == null)
		{
			System.out.println("No user found");
			loggy.warn("No user found with the username: " + userN);
			return null;
		}
		else
		{
			if(selectUser.getPassword().equals(pass) == false)
			{
				System.out.println("Password incorrect");
				loggy.warn("Password for " + userN + "entered incorrectly.");
				return null;
				
			}
			else
			{
				System.out.println("Login Successful.");
				loggy.info("Successful login for " + selectUser.getUsername());
				
				//clear password before it gets sent.
				selectUser.setPassword("");
				return selectUser;
			}
		}
		
	}

	@Override
	public int checkUserRole(User u) 
	{
		User chkUser = uDAO.selectUserbyUserID(u.getUserId());
		loggy.info(chkUser.getUsername() + " role is " + chkUser.getRoleName());
		return chkUser.getRoleId();
		
	}

}
