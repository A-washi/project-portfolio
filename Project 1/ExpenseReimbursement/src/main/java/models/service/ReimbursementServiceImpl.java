package models.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import models.Reimbursement;
import models.User;
import models.dao.ReimbursementDAO;
import models.dao.ReimbursementDAOImpl;


public class ReimbursementServiceImpl implements ReimbursementService 
{
	ReimbursementDAO rDao = new ReimbursementDAOImpl();
	final static Logger loggy = Logger.getLogger(ReimbursementServiceImpl.class);
	
	//For H2 testing
	public ReimbursementServiceImpl() 
	{
	
	}
	
	//For H2 testing
	public ReimbursementServiceImpl(ReimbursementDAO psuedoDao)
	{
		rDao = psuedoDao;
	}

	@Override
	public List<Reimbursement> getAllReimbursements() 
	{

		List<Reimbursement> rList = rDao.selectAllReimbursements();

		if(rList == null || rList.isEmpty())
		{
			System.out.println("There are no reimbursements in the system");
			loggy.warn("There were no reimbursements found.");
			return new ArrayList<Reimbursement>();
		}
		else
		{
			loggy.info("Returning all reimbursements.");
			return rList;
		}

	}

	@Override
	public List<Reimbursement> getAllReimbursementsbyAuthor(User author) 
	{
		List<Reimbursement> rList = rDao.selectAllReimbursementsByAuthor(author);

		if(rList == null || rList.isEmpty())
		{
			System.out.println("No reimbursements found.");
			loggy.warn("There were no reimbursements found for: " + author);
			return new ArrayList<Reimbursement>();
		}
		else
		{
			loggy.info("Returning all reimbursements for " + author);
			return rList;
		}
	}

	@Override
	public List<Reimbursement> getAllReimbursementsbyResolver(User resolver) 
	{
		List<Reimbursement> rList = rDao.selectAllReimbursementsByResolver(resolver);

		if(rList == null || rList.isEmpty())
		{
			System.out.println("No reimbursements found.");
			loggy.warn("There were no reimbursements found with this resolver: " + resolver);
			return new ArrayList<Reimbursement>();
		}
		else
		{
			loggy.info("Returning all reimbursements with resolver, " + resolver);
			return rList;
		}
	}

	@Override
	public List<Reimbursement> getAllReimbursementsByStatus(int statusID) 
	{
		List<Reimbursement> rList = rDao.selectAllReimbursementsByStatus(statusID);

		if(rList == null || rList.isEmpty())
		{
			System.out.println("No reimbursements found.");
			loggy.warn("There were no reimbursements found with this statusID: " + statusID);
			return new ArrayList<Reimbursement>();
		}
		else
		{
			loggy.info("Returning all reimbursements with the status ID " + statusID);
			return rList;
		}
	}

	@Override
	public boolean addNewReimbursement(Reimbursement reimb) 
	{
		if(reimb.getAuthor() == null)
		{
			System.out.println("This reimbursement doesn't have an author and will not be added to the system.");
			loggy.error(reimb + " does not have an author and will not be added.");
			return false;
		}
		else
		{

			if(rDao.insertReimbursement(reimb))
			{
				System.out.println("Reimbursement successfully added.");
				loggy.info(reimb + " successfully added to database.");
				return true;
			}
			else
			{
				System.out.println("Reimbursement was unable to be added.");
				loggy.warn(reimb + " was not added to the database.");
				return false;
			}
		}

	}

	@Override
	public boolean resolveReimbursement(Reimbursement reimb, User resolver) 
	{
		reimb.setResolver(resolver);
		
		
		if(reimb.getResolver() == null)
		{
			System.out.println("This reimbursement doesn't have a resolver being added to it and will not be marked as resolved.");
			loggy.error(reimb + " was not given a resolver and will not be updated.");
			return false;
		}
		else
		{

			if(rDao.updateReimbursementAsResolved(reimb))
			{
				System.out.println("Reimbursement successfully resolved.");
				loggy.info(reimb + " successfully updated as resolved by " + reimb.getResolver().getUsername() + ".");
				return true;
			}
			else
			{
				System.out.println("Reimbursement was not updated.");
				loggy.warn(reimb + " was not updated in the database.");
				return false;
			}
		}

	}

	/***
	 * Approves a reimbursement adding the resolver, setting the resolution date to NOW(), and changing status to approved. Returns true if updated any rows. <br>
	 * Not currently used. Overloaded.
	 * 
	 * @param reimb The reimbursement to be found
	 * @param resolver the finance manager that is approving.
	 * @returns boolean true if row is updated.
	 * @Override
	 */
	public boolean approveReimbursement(Reimbursement reimb, User resolver) 
	{
		if(resolver.getRoleId() != 2)
		{
			System.out.println("The current user does not have the permission to approve reimbursements");
			loggy.error(resolver + " is trying to approve a reimbursement without the right role: "+ resolver.getRoleName());
			return false;
		}
		else
		{
			reimb.setStatusID(2);
			if(rDao.updateReimbursement(reimb))
			{
				System.out.println("Reimbursement successfully updated.");
				loggy.info(reimb + " successfully updated as resolved by " + reimb.getResolver().getUsername() + ".");
				return true;
			}
			else
			{
				System.out.println("Reimbursement was not updated.");
				loggy.warn(reimb + " was not updated in the database.");
				return false;
			}
		}
	}

	/***
	 * Denies a reimbursement adding the resolver, setting the resolution date to NOW(), and changing status to denied. Returns true if updated any rows. <br>
	 * Not currently used. Overloaded.
	 * 
	 * @param reimb The reimbursement to be found.
	 * @param resolver the finance manager that is denying.
	 * @returns boolean true if row is updated.
	 * @Override
	 */
	
	public boolean denyReimbursement(Reimbursement reimb, User resolver) 
	{
		if(resolver.getRoleId() != 2)
		{
			System.out.println("The current user does not have the permission to deny reimbursements");
			loggy.error(resolver + " is trying to deny a reimbursement without the right role: "+ resolver.getRoleName());
			return false;
		}
		else
		{
			reimb.setStatusID(3); 
			if(rDao.updateReimbursement(reimb))
			{
				System.out.println("Reimbursement successfully updated.");
				loggy.info(reimb + " successfully updated as resolved by " + reimb.getResolver().getUsername() + ".");
				return true;
			}
			else
			{
				System.out.println("Reimbursement was not updated.");
				loggy.warn(reimb + " was not updated in the database.");
				return false;
			}
		}
	}
	
	/***
	 * Approves a reimbursement adding the resolver, setting the resolution date to NOW(), and changing status to approved. Returns true if updated any rows. <br>
	 * In use. Overloaded.
	 * 
	 * @param reimbID The reimbursement ID to be found
	 * @param resolver the finance manager that is approving.
	 * @returns boolean true if row is updated.
	 * @Override
	 */
	public boolean approveReimbursement(int reimbID, User resolver) 
	{
		if(resolver.getRoleId() != 2)
		{
			System.out.println("The current user does not have the permission to approve reimbursements");
			loggy.error(resolver + " is trying to approve a reimbursement without the right role: "+ resolver.getRoleName());
			return false;
		}
		else
		{
			
			if(rDao.approveReimbursement(reimbID, resolver))
			{
				System.out.println("Reimbursement successfully updated.");
				loggy.info(reimbID + " successfully updated as resolved by " + resolver.getUsername() + ".");
				return true;
			}
			else
			{
				System.out.println("Reimbursement was not updated.");
				loggy.warn(reimbID + " was not updated in the database.");
				return false;
			}
		}
	}

	/***
	 * Denies a reimbursement adding the resolver, setting the resolution date to NOW(), and changing status to denied. Returns true if updated any rows. <br>
	 * In use. Overloaded.
	 * 
	 * @param reimb The reimbursement to be found.
	 * @param resolver the finance manager that is denying.
	 * @returns boolean true if row is updated.
	 * @Override
	 */
	public boolean denyReimbursement(int reimbID, User resolver) 
	{
		if(resolver.getRoleId() != 2)
		{
			System.out.println("The current user does not have the permission to deny reimbursements");
			loggy.error(resolver + " is trying to approve a reimbursement without the right role: "+ resolver.getRoleName());
			return false;
		}
		else
		{
			
			if(rDao.denyReimbursement(reimbID, resolver))
			{
				System.out.println("Reimbursement successfully updated.");
				loggy.info(reimbID + " successfully updated as resolved by " + resolver.getUsername() + ".");
				return true;
			}
			else
			{
				System.out.println("Reimbursement was not updated.");
				loggy.warn(reimbID + " was not updated in the database.");
				return false;
			}
		}
	}



	@Override
	public boolean setReimbursementToPending(Reimbursement reimb, User resolver) 
	{
		if(resolver.getRoleId() != 2)
		{
			System.out.println("The current user does not have the permission to change the status of reimbursements");
			loggy.error(resolver + " is trying to set a reimbursement to pending without the right role: "+ resolver.getRoleName());
			return false;
		}
		else
		{
			reimb.setStatusID(1); 
			if(rDao.updateReimbursement(reimb))
			{
				System.out.println("Reimbursement successfully updated.");
				loggy.info(reimb + " successfully updated by " + reimb.getResolver().getUsername() + ".");
				return true;
			}
			else
			{
				System.out.println("Reimbursement was not updated.");
				loggy.warn(reimb + " was not updated in the database.");
				return false;
			}
		}

	}

	@Override
	public boolean removeReimbursement(Reimbursement reimb, User resolver) 
	{
		if(resolver.getRoleId() != 2)
		{
			System.out.println("The current user does not have the permission to delete reimbursements");
			loggy.error(resolver + " is trying to delete a reimbursement without the right role: "+ resolver.getRoleName());
			return false;
		}
		else
		{ 
			if(rDao.deleteReimbursement(reimb))
			{
				System.out.println("Reimbursement successfully removed.");
				loggy.warn(reimb + " successfully removed by " + reimb.getResolver().getUsername() + ".");
				return true;
			}
			else
			{
				System.out.println("Reimbursement was not deleted.");
				loggy.warn(reimb + " was not deleted in the database.");
				return false;
			}
		}

	}

	//For testing purposes.
	@Override
	public void displayReimbursement(Reimbursement reimb) 
	{
		System.out.println("\nReimbursement ID: " + reimb.getId());
		System.out.println("\nAmount: " + reimb.getAmount());
		System.out.println("\nDescription: " + reimb.getDescription());
		System.out.println("\nSubmit Date: " + reimb.getSubmitDate());
		System.out.println("\nResolution Date: " + reimb.getResolutionDate());
		System.out.println("\nStatus: " + reimb.getStatusName());
		System.out.println("\nType: " + reimb.getTypeName());
		System.out.println("\nSubmitter: " + reimb.getAuthor().getFirstName() + " " + reimb.getAuthor().getLastName());
		System.out.println("\nSubmitter username: " + reimb.getAuthor().getUsername());

	}

}
