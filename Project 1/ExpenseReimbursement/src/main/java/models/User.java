package models;

import org.apache.log4j.Logger;

/**
 * Mirrors the User table. <br>
 * Role IDs:  1.Employee, 2.Finance Manager (FinManager in DB)
 * @author Albert
 *
 */
public class User 
{
	final static Logger loggy = Logger.getLogger(User.class);
	
	private int userId;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private int roleId;
	private String roleName;//Role name should be set by the id setter.
	

	

	public User(int userId, String username, String password, String email, int roleId, String firstName, String lastName) 
	{
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.email = email;
		setRoleId(roleId);
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserId() {
		return userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRoleName() 
	{
		return roleName;
	}
	
	//Role name should be set by the set role id.
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	
	public int getRoleId() {
		return roleId;
	}

	/***
	 * Will automatically set role name based input. <br> 1 = Employee, 2 = Finance Manager
	 * @param roleId
	 */
	public void setRoleId(int roleId) {
		
		this.roleId = roleId;
		
		switch (roleId) 
		{
		case 1:
			this.roleName = "Employee";
			break;

		case 2:
			this.roleName = "Finance Manager";
			break;
			
		default:
			loggy.error(this.username + " was not given a valid role!");
			break;
		}

		
	}


	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + ", email=" + email + ", role=" + roleName + "]";
	}
	
	
	
}
