package models;

import java.io.Serializable;

import org.apache.log4j.Logger;

/**
 * Mirrors the reimbursement table. <br>
 * Status IDs: 1 = Pending, 2 = Approved, 3 = Denied <br>
 * Type IDs: 1 = Lodging, 2 = Travel, 3 = Food, 4 = Other
 * @author Albert
 * 
 *
 */
public class Reimbursement 
{


	final static Logger loggy = Logger.getLogger(Reimbursement.class);
	
	private int id;
	private double amount;
	private String submitDate;
	private String resolutionDate = "";
	private String description = "";
	private byte[] receipt;
	private User author;
	private User resolver;
	private int statusID;
	private String statusName;
	private int typeID;
	private String typeName;
	
	
	public Reimbursement(double amount, String submitDate, User author, int statusID, int typeID) 
	{
		this.amount = amount;
		this.submitDate = submitDate;
		this.author = author;
		setStatusID(statusID);
		setTypeID(typeID);
	}
	
	public Reimbursement(double amount, User author, int statusID, int typeID) 
	{
		this.amount = amount;
		this.author = author;
		setStatusID(statusID);
		setTypeID(typeID);
	}
	
	public Reimbursement(double amount, String submitDate, User author, int typeID) 
	{
		this.amount = amount;
		this.submitDate = submitDate;
		this.author = author;
		setStatusID(1);
		setTypeID(typeID);
	}
	
	public Reimbursement(double amount, User author, int typeID) 
	{
		this.amount = amount;
		this.author = author;
		setStatusID(1);
		setTypeID(typeID);
	}
	
	
	
	
	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}
	public String getResolutionDate() {
		return resolutionDate;
	}
	public void setResolutionDate(String resolutionDate) {
		this.resolutionDate = resolutionDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public byte[] getReceipt() {
		return receipt;
	}
	public void setReceipt(byte[] receipt) {
		this.receipt = receipt;
	}
	public User getAuthor() {
		return author;
	}
	public void setAuthor(User author) {
		this.author = author;
	}
	public User getResolver() {
		return resolver;
	}
	public void setResolver(User resolver) {
		this.resolver = resolver;
	}

	public int getStatusID() {
		return statusID;
	}

	/***
	 * Will automatically set status name based input. <br> 
	 * 1 = Pending, 2 = Approved, 3 = Denied
	 * @param statusID
	 */
	
	public void setStatusID(int statusID) 
	{
		this.statusID = statusID;
		
		
		switch (statusID) 
		{
		case 1:
			this.statusName = "Pending";
			break;

		case 2:
			this.statusName = "Approved";
			break;
			
		case 3:
			this.statusName = "Denied";
			break;
			
		default:
			loggy.error("Reimbursement: " + this.id + " was not given a valid status!");
			break;
		}
	}
	

	public int getTypeID() {
		return typeID;
	}

	/***
	 * Will automatically set type name based input. <br> 
	 * 1 = Lodging, 2 = Travel, 3 = Food, 4 = Other
	 * @param typeID
	 */
	public void setTypeID(int typeID) 
	{
		this.typeID = typeID;
		switch (typeID) 
		{
		case 1:
			this.typeName = "Lodging";
			break;

		case 2:
			this.typeName = "Travel";
			break;
			
		case 3:
			this.typeName = "Food";
			break;
			
		case 4:
			this.typeName = "Other";
			
		default:
			loggy.error("Reimbursement: " + this.id + " was not given a valid type!");
			break;
		}
	}

	public String getStatusName() {
		return statusName;
	}

	public String getTypeName() {
		return typeName;
	}

	

	
	
	
}
