package models.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import models.Reimbursement;
import models.User;

public class ReimbursementDAOImpl implements ReimbursementDAO 
{
	final static Logger loggy = Logger.getLogger(UsersDAOImpl.class);
	
	public static String url = System.getenv("REVATURE_DB_URL") + "ExpenseReimbursement";
	public static String username= System.getenv("REVATURE_DB_USERNAME");
	public static String password= System.getenv("REVATURE_DB_PASSWORD");
	
	//For H2 testing
	public ReimbursementDAOImpl() 
	{
	}
	
	public ReimbursementDAOImpl(String _url, String _username, String _password) 
	{
		url= _url;
		username = _username;
		password = _password;
	}

	/**
	 * get all reimbursements in the system and constructs reimbursement objects for them.
	 * @returns List<Reimbursement>
	 * @Override
	 */
	public List<Reimbursement> selectAllReimbursements() 
	{
		List<Reimbursement> rList = new ArrayList<Reimbursement>();
		
		loggy.info("Attempting to query database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "SELECT r.*, rs.status_name, rt.type_name, \r\n" + 
					"	ru1.username AS a_username, ru1.\"password\" AS a_password, ru1.first_name AS a_first, ru1.last_name AS a_last, ru1.email AS a_email, ru1.role_id AS a_role, \r\n" + 
					"	ru2.username AS r_username, ru2.\"password\" AS r_password, ru2.first_name AS r_first, ru2.last_name AS r_last, ru2.email AS r_email, ru2.role_id AS r_role \r\n" + 
					"FROM reimbursement r\r\n" + 
					"	LEFT JOIN reimbursement_status rs \r\n" + 
					"	ON r.status_id = rs.status_id\r\n" + 
					"	LEFT JOIN reimbursement_type rt \r\n" + 
					"	ON r.type_id = rt.type_id\r\n" + 
					"	LEFT JOIN reimb_user ru1 \r\n" + 
					"	ON r.author = ru1.user_id\r\n" + 
					"	LEFT JOIN reimb_user ru2\r\n" + 
					"	ON r.resolver = ru2.user_id;";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			
			while(rs.next())
			{
				//Construct the submitter user
				User authorUser = 
						new User(rs.getInt("author"), rs.getString("a_username"), rs.getString("a_password"), rs.getString("a_email"), rs.getInt("a_role"), rs.getString("a_first"), rs.getString("a_last"));
						
				
				//Construct the reimbursement
				Reimbursement nxtReimb = 
						new Reimbursement(rs.getDouble("amount"), rs.getString("submitted"), authorUser, rs.getInt("status_id"), rs.getInt("type_id"));
				nxtReimb.setId(rs.getInt(1));
				nxtReimb.setDescription(rs.getString("description"));
				
				//Construct the resolver if there is one.
				rs.getInt("resolver");//get the column so that it can be tested with was null
				if(rs.wasNull() == false)
				{
					User resolverUser =
							new User(rs.getInt("resolver"), rs.getString("r_username"), rs.getString("r_password"), rs.getString("r_email"), rs.getInt("r_role"), rs.getString("r_first"), rs.getString("r_last"));

					nxtReimb.setResolver(resolverUser);
					nxtReimb.setResolutionDate(rs.getString("resolved"));
				}

				rList.add(nxtReimb);
			}
			
			loggy.info("Accessed list of reimbursements.");
			return rList;
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return null;
		}
	}

	/**
	 * get all reimbursements in the system were submitted by the specified user and constructs reimbursement objects for them.
	 * @returns List<Reimbursement>
	 * @param User author user that the reimbursement is tied to
	 * @Override
	 */
	public List<Reimbursement> selectAllReimbursementsByAuthor(User author) 
	{
		List<Reimbursement> rList = new ArrayList<Reimbursement>();
		
		loggy.info("Attempting to query database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			//This incorporates more joins than is needed since the author user is an argument
			String sql = "SELECT r.*, rs.status_name, rt.type_name, \r\n" + 
					"	ru1.username AS a_username, ru1.\"password\" AS a_password, ru1.first_name AS a_first, ru1.last_name AS a_last, ru1.email AS a_email, ru1.role_id AS a_role, \r\n" + 
					"	ru2.username AS r_username, ru2.\"password\" AS r_password, ru2.first_name AS r_first, ru2.last_name AS r_last, ru2.email AS r_email, ru2.role_id AS r_role \r\n" + 
					" FROM reimbursement r\r\n" + 
					"	LEFT JOIN reimbursement_status rs \r\n" + 
					"	ON r.status_id = rs.status_id\r\n" + 
					"	LEFT JOIN reimbursement_type rt \r\n" + 
					"	ON r.type_id = rt.type_id\r\n" + 
					"	LEFT JOIN reimb_user ru1 \r\n" + 
					"	ON r.author = ru1.user_id\r\n" + 
					"	LEFT JOIN reimb_user ru2\r\n" + 
					"	ON r.resolver = ru2.user_id" +
					" WHERE author = ?"; 
			
			PreparedStatement ps = conn.prepareStatement(sql);
			

			ps.setInt(1, author.getUserId());
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{			
				//Construct reimbursement
				Reimbursement nxtReimb = new Reimbursement(rs.getBigDecimal(2).doubleValue(), rs.getString(3), author, rs.getInt(9), rs.getInt(10));
				nxtReimb.setId(rs.getInt(1));
				nxtReimb.setDescription(rs.getString("description"));

				
				rs.getInt("resolver"); //get the column so that it can be tested with was null
				if(rs.wasNull() == false)
				{
					//Construct the resolver if there is one.
					User resolverUser =
							new User(rs.getInt("resolver"), rs.getString("r_username"), rs.getString("r_password"), rs.getString("r_email"), rs.getInt("r_role"), rs.getString("r_first"), rs.getString("r_last"));

					nxtReimb.setResolver(resolverUser);
					nxtReimb.setResolutionDate(rs.getString("resolved"));
				}

				rList.add(nxtReimb);
			}
			
			loggy.info("Accessed list of reimbursements.");
			return rList;
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return rList;
		}
	}

	/**
	 * get all reimbursements in the system were resolved by the specified user and constructs reimbursement objects for them. <br>
	 * 
	 * @returns List<Reimbursement>
	 * @param User resolver user that the resolution is tied to
	 * @Override
	 */
	public List<Reimbursement> selectAllReimbursementsByResolver(User resolver) 
	{
		List<Reimbursement> rList = new ArrayList<Reimbursement>();
		
		loggy.info("Attempting to query database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			//This incorporates more joins than is needed since the author user is an argument
			String sql = "SELECT r.*, rs.status_name, rt.type_name, \r\n" + 
					"	ru1.username AS a_username, ru1.\"password\" AS a_password, ru1.first_name AS a_first, ru1.last_name AS a_last, ru1.email AS a_email, ru1.role_id AS a_role, \r\n" + 
					"	ru2.username AS r_username, ru2.\"password\" AS r_password, ru2.first_name AS r_first, ru2.last_name AS r_last, ru2.email AS r_email, ru2.role_id AS r_role \r\n" + 
					"FROM reimbursement r\r\n" + 
					"	LEFT JOIN reimbursement_status rs \r\n" + 
					"	ON r.status_id = rs.status_id\r\n" + 
					"	LEFT JOIN reimbursement_type rt \r\n" + 
					"	ON r.type_id = rt.type_id\r\n" + 
					"	LEFT JOIN reimb_user ru1 \r\n" + 
					"	ON r.author = ru1.user_id\r\n" + 
					"	LEFT JOIN reimb_user ru2\r\n" + 
					"	ON r.resolver = ru2.user_id"
					+ " WHERE resolver = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, resolver.getUserId());
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				
				User authorUser = 
						new User(rs.getInt("author"), rs.getString("a_username"), rs.getString("a_password"), rs.getString("a_email"), rs.getInt("a_role"), rs.getString("a_first"), rs.getString("a_last"));
				
				
				Reimbursement nxtReimb = 
						new Reimbursement(rs.getDouble("amount"), rs.getString("submitted"), authorUser, rs.getInt("status_id"), rs.getInt("type_id"));
				
				nxtReimb.setId(rs.getInt(1));
				nxtReimb.setDescription(rs.getString("description"));
				
				//get the column so that it can be tested with was null
				rs.getInt("resolver");
				if(rs.wasNull() == false)
				{
					User resolverUser =
							new User(rs.getInt("resolver"), rs.getString("r_username"), rs.getString("r_password"), rs.getString("r_email"), rs.getInt("r_role"), rs.getString("r_first"), rs.getString("r_last"));

					nxtReimb.setResolver(resolverUser);
					nxtReimb.setResolutionDate(rs.getString("resolved"));
				}

				rList.add(nxtReimb);
			}
			
			loggy.info("Accessed list of reimbursements.");
			return rList;
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return rList;
		}
	}

	
	/**
	 * get all reimbursements in the system by the specified status and constructs reimbursement objects for them. <br>
	 * 
	 * @returns List<Reimbursement>
	 * @param int StatusID The status to search by (1 = Pending, 2 = Approved, 3 = Denial)
	 * @Override
	 */
	public List<Reimbursement> selectAllReimbursementsByStatus(int statusID) 
	{
		List<Reimbursement> rList = new ArrayList<Reimbursement>();
		
		loggy.info("Attempting to query database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			
			String sql = "SELECT r.*, rs.status_name, rt.type_name, \r\n" + 
					"	ru1.username AS a_username, ru1.\"password\" AS a_password, ru1.first_name AS a_first, ru1.last_name AS a_last, ru1.email AS a_email, ru1.role_id AS a_role, \r\n" + 
					"	ru2.username AS r_username, ru2.\"password\" AS r_password, ru2.first_name AS r_first, ru2.last_name AS r_last, ru2.email AS r_email, ru2.role_id AS r_role \r\n" + 
					"FROM reimbursement r\r\n" + 
					"	LEFT JOIN reimbursement_status rs \r\n" + 
					"	ON r.status_id = rs.status_id\r\n" + 
					"	LEFT JOIN reimbursement_type rt \r\n" + 
					"	ON r.type_id = rt.type_id\r\n" + 
					"	LEFT JOIN reimb_user ru1 \r\n" + 
					"	ON r.author = ru1.user_id\r\n" + 
					"	LEFT JOIN reimb_user ru2\r\n" + 
					"	ON r.resolver = ru2.user_id"
					+ " WHERE status_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, statusID);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				//This is unneeded as we should already have the Author, but it's being kept out of paranoia.
				User authorUser = 
						new User(rs.getInt("author"), rs.getString("a_username"), rs.getString("a_password"), rs.getString("a_email"), rs.getInt("a_role"), rs.getString("a_first"), rs.getString("a_last"));
				
				
				Reimbursement nxtReimb = 
						new Reimbursement(rs.getDouble("amount"), rs.getString("submitted"), authorUser, rs.getInt("status_id"), rs.getInt("type_id"));
				nxtReimb.setId(rs.getInt(1));
				nxtReimb.setDescription(rs.getString("description"));
				
				//get the column so that it can be tested with was null
				rs.getInt("resolver");
				if(rs.wasNull() == false)
				{
					User resolverUser =
							new User(rs.getInt("resolver"), rs.getString("r_username"), rs.getString("r_password"), rs.getString("r_email"), rs.getInt("r_role"), rs.getString("r_first"), rs.getString("r_last"));

					nxtReimb.setResolver(resolverUser);
					nxtReimb.setResolutionDate(rs.getString("resolved"));
				}

				rList.add(nxtReimb);
			}
			
			loggy.info("Accessed list of reimbursements.");
			return rList;
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return rList;
		}
	}

	/**
	 * get all reimbursements in the system by the specified reimbursement ID and constructs objects for them. <br>
	 * 
	 * @returns Reimbursement
	 * @param int ID The ID of the reimbursment to be found.
	 * @Override
	 */
	public Reimbursement selectReimbursementByID(int id) 
	{
		Reimbursement selected;
		
		loggy.info("Attempting to query database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			//This incorporates more joins than is needed since the author user is an argument
			String sql = "SELECT r.*, rs.status_name, rt.type_name, \r\n" + 
					"	ru1.username AS a_username, ru1.\"password\" AS a_password, ru1.first_name AS a_first, ru1.last_name AS a_last, ru1.email AS a_email, ru1.role_id AS a_role, \r\n" + 
					"	ru2.username AS r_username, ru2.\"password\" AS r_password, ru2.first_name AS r_first, ru2.last_name AS r_last, ru2.email AS r_email, ru2.role_id AS r_role \r\n" + 
					"FROM reimbursement r\r\n" + 
					"	LEFT JOIN reimbursement_status rs \r\n" + 
					"	ON r.status_id = rs.status_id\r\n" + 
					"	LEFT JOIN reimbursement_type rt \r\n" + 
					"	ON r.type_id = rt.type_id\r\n" + 
					"	LEFT JOIN reimb_user ru1 \r\n" + 
					"	ON r.author = ru1.user_id\r\n" + 
					"	LEFT JOIN reimb_user ru2\r\n" + 
					"	ON r.resolver = ru2.user_id"
					+ " WHERE reimb_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				//This is unneeded as we should already have the Author, but it's being kept out of paranoia.
				User authorUser = 
						new User(rs.getInt("author"), rs.getString("a_username"), rs.getString("a_password"), rs.getString("a_email"), rs.getInt("a_role"), rs.getString("a_first"), rs.getString("a_last"));
				
				
				Reimbursement nxtReimb = 
						new Reimbursement(rs.getDouble("amount"), rs.getString("submitted"), authorUser, rs.getInt("status_id"), rs.getInt("type_id"));
				nxtReimb.setId(rs.getInt(1));
				nxtReimb.setDescription(rs.getString("description"));
				
				//get the column so that it can be tested with was null
				rs.getInt("resolver");
				if(rs.wasNull() == false)
				{
					User resolverUser =
							new User(rs.getInt("resolver"), rs.getString("r_username"), rs.getString("r_password"), rs.getString("r_email"), rs.getInt("r_role"), rs.getString("r_first"), rs.getString("r_last"));

					nxtReimb.setResolver(resolverUser);
					nxtReimb.setResolutionDate(rs.getString("resolved"));
					
				}
				
				selected = nxtReimb;
				loggy.info("Reimbursement " +selected.getId()+ " found.");
				return selected;
			}
			
			loggy.warn("Reimbursement not found or too many were found.");
			return null;
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return null;
		}
	}

	/**
	 * Insert a new reimbursement to the system 
	 * 
	 * @returns boolean true if successfully inserted something.
	 * @param Reimbursement reimb Reimbursement to insert
	 * @Override
	 */
	public boolean insertReimbursement(Reimbursement reimb) 
	{
		
		loggy.info("Attempting to update database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			//New records are automatically set to pending
			String sql = "INSERT INTO reimbursement (amount, submitted, description, author, status_id, type_id)\r\n" + 
					"	VALUES (?, NOW(), ?, ?, 1, ?)";
			
			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setDouble(1, reimb.getAmount());
			ps.setString(2, reimb.getDescription());
			ps.setInt(3, reimb.getAuthor().getUserId());
			ps.setInt(4, reimb.getTypeID());
			
			int count = ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			
			while(rs.next())
			{
				
				reimb.setId(rs.getInt(1));
			}
			
			if(count > 0)
			{
				loggy.info("Reimbursement " +reimb.getId()+ " added.");
				return true;
			}
			else
			{
				loggy.info("Reimbursement wasn't added.");
				return false;
			}
			
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}
	}

	/**
	 * General update for a reimbursement to the system. <br>
	 * Will set the amount, description, receipt, author, resolver, status id, type id, and resolution date 
	 * regardless if they were changed.
	 * 
	 * @returns boolean true if successfully updated something.
	 * @param Reimbursement reimb Reimbursement to be updated
	 * @Override
	 */
	public boolean updateReimbursement(Reimbursement reimb) 
	{
		loggy.info("Attempting to update the database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "UPDATE reimbursement"
					+ " SET amount = ?, description = ?, receipt = ?"
					+ ", author = ?, resolver = ?, status_id = ?, type_id = ?, resolved = ?"
					+ " WHERE reimb_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setDouble(1, reimb.getAmount());
			ps.setString(2, reimb.getDescription());
			ps.setBytes(3, reimb.getReceipt());
			ps.setInt(4, reimb.getAuthor().getUserId());
			ps.setInt(5, reimb.getResolver().getUserId());
			ps.setInt(6, reimb.getStatusID());
			ps.setInt(7, reimb.getTypeID());
			ps.setString(8, reimb.getResolutionDate());
			ps.setInt(9, reimb.getId());
			
						
			int count = ps.executeUpdate();
			
			if(count > 0)
			{
				loggy.info("Reimbursement " + reimb.getId() + " successfully updated.");
				return true;
			}
			else
			{
				loggy.info("Reimbursement " + reimb.getId() + " was not updated.");
				return false;
			}
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}
	}
	
	/**
	 * Approve given reimbursement with given resolver. Resolution date will be set to the current time. <br>
	 * 
	 * @returns boolean true if successfully updated something.
	 * @param Reimbursement reimb Reimbursement to be approved.
	 * @param User Resolver user that is responsibility for the resolution.
	 * @Override
	 */
	public boolean approveReimbursement(int reimb, User resolver) 
	{
		loggy.info("Attempting to update the database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "UPDATE reimbursement"
					+ " SET resolved = NOW(), resolver = ?, status_id = 2"
					+ " WHERE reimb_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, resolver.getUserId());
			ps.setInt(2, reimb);
			
						
			int count = ps.executeUpdate();
			
			if(count > 0)
			{
				loggy.info("Reimbursement " + reimb + " successfully approved.");
				return true;
			}
			else
			{
				loggy.info("Reimbursement " + reimb + " was not approved despite attempt.");
				return false;
			}
		}
		catch(SQLException e)
		{
			
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}
	}
	
	/**
	 * Deny given reimbursement with given resolver. Resolution date will be set to the current time. <br>
	 * 
	 * @returns boolean true if successfully updated something.
	 * @param Reimbursement reimb Reimbursement to be denied.
	 * @param User Resolver user that is responsibility for the resolution.
	 * @Override
	 */

	public boolean denyReimbursement(int reimb, User resolver) 
	{
		loggy.info("Attempting to update the database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "UPDATE reimbursement"
					+ " SET resolved = NOW(), resolver = ?, status_id = 3"
					+ " WHERE reimb_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, resolver.getUserId());
			ps.setInt(2, reimb);
			
						
			int count = ps.executeUpdate();
			
			if(count > 0)
			{
				loggy.info("Reimbursement " + reimb + " successfully denied.");
				return true;
			}
			else
			{
				loggy.info("Reimbursement " + reimb + " was not denied despite attempt.");
				return false;
			}
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}	

	}

	/**
	 * Deletes a given reimbursement. <br>
	 * 
	 * @returns boolean true if successfully updated something.
	 * @param Reimbursement reimb Reimbursement to be deleted.
	 * @Override
	 */
	public boolean deleteReimbursement(Reimbursement reimb) 
	{
		loggy.info("Attempting to update the database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "DELETE FROM reimbursement"
					+ " WHERE reimb_id = ?";
			
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimb.getId());
			
			
			int count = ps.executeUpdate();
			
			if(count > 0)
			{
				loggy.info("Reimbursement " + reimb.getId() + " was succesfully deleted.");
				return true;
			}
			else
			{
				loggy.info("Reimbursement " + reimb.getId() + " was not deleted.");
				return false;
			}
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}
	}

	/**
	 * Resolves a reimbursement without updating the status ID <br>
	 * 
	 * @returns boolean true if successfully updated something.
	 * @param Reimbursement reimb Reimbursement to be updated.
	 * @Override
	 */
	public boolean updateReimbursementAsResolved(Reimbursement reimb) 
	{
		loggy.info("Attempting to update the database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "UPDATE reimbursement"
					+ " SET resolved = NOW(), SET resolver = ?"
					+ " WHERE reimb_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimb.getResolver().getUserId());
			ps.setInt(2, reimb.getId());
			
						
			int count = ps.executeUpdate();
			
			if(count > 0)
			{
				loggy.info("Reimbursement " + reimb.getId() + " successfully resolved.");
				return true;
			}
			else
			{
				loggy.info("Reimbursement " + reimb.getId() + " was not resolved.");
				return false;
			}
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}
	}

	/***
	 * H2 Testing create DB
	 */
	@Override
	public void h2InitDao() 
	{
		try(Connection conn = DriverManager.getConnection(url,username, password))
		{
			String sql= "CREATE TABLE reimbursement(\r\n" + 
					"	reimb_id SERIAL PRIMARY KEY\r\n" + 
					"	, amount DECIMAL(30,2) NOT NULL\r\n" + 
					"	, submitted TIMESTAMP NOT NULL DEFAULT NOW()\r\n" + 
					"	, resolved TIMESTAMP \r\n" + 
					"	, description VARCHAR(250)\r\n" + 
					"	, receipt BYTEA NULL\r\n" + 
					"	, author INTEGER NOT NULL\r\n" + 
					"	, resolver INTEGER NULL\r\n" + 
					"	, status_id INTEGER NULL\r\n" + 
					"	, type_id INTEGER NULL );";
			
			Statement state = conn.createStatement();
			state.execute(sql);
			
		}
		catch(SQLException e) 
		{
			e.printStackTrace();
		}
		
	}

	/***
	 * H2 Testing drop DB
	 */
	@Override
	public void h2DestroyDao() 
	{
		try(Connection conn = DriverManager.getConnection(url,username, password))
		{
			String sql= "DROP TABLE reimbursement;";
			
			Statement state = conn.createStatement();
			state.execute(sql);
		}
		catch(SQLException e) 
		{
			e.printStackTrace();
		}
		
	}

	
}
