package models.dao;

import java.util.List;

import models.Reimbursement;
import models.User;

public interface ReimbursementDAO 
{
	//SELECT
	public List<Reimbursement> selectAllReimbursements();
	
	public List<Reimbursement> selectAllReimbursementsByAuthor(User author);
	
	public List<Reimbursement> selectAllReimbursementsByResolver(User resolver);
	
	public List<Reimbursement> selectAllReimbursementsByStatus(int statusID);
	
	public Reimbursement selectReimbursementByID(int id);
	
	//INSERT
	public boolean insertReimbursement(Reimbursement reimb);
	
	//UPDATE
	public boolean updateReimbursement(Reimbursement reimb);
	
	public boolean updateReimbursementAsResolved(Reimbursement reimb);
	
	public boolean approveReimbursement(int reimb, User resolver);

	public boolean denyReimbursement(int reimb, User resolver);
	
	//DELETE
	public boolean deleteReimbursement(Reimbursement reimb);
	
	//For H2 testing
	public void h2InitDao();
	public void h2DestroyDao();
	
}
