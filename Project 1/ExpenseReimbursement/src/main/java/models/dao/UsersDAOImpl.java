package models.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import models.User;


public class UsersDAOImpl implements UsersDAO 
{
	final static Logger loggy = Logger.getLogger(UsersDAOImpl.class);
	
	public static String url = System.getenv("REVATURE_DB_URL") + "ExpenseReimbursement";
	public static String username= System.getenv("REVATURE_DB_USERNAME");
	public static String password= System.getenv("REVATURE_DB_PASSWORD");
	
	//For H2 testing
	public UsersDAOImpl() 
	{
	}
	
	public UsersDAOImpl(String _url, String _username, String _password) 
	{
		url= _url;
		username = _username;
		password = _password;
	}


	/**
	 * Selects all users in DB. <br>
	 * 
	 * @returns List<User>
	 * @Override
	 */
	public List<User> selectAllUsers() 
	{
		List<User> uList = new ArrayList<User>();
		
		loggy.info("Attempting to query database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "SELECT * FROM reimb_user";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				
				User nxtUser = 
						new User(rs.getInt("user_id"), rs.getString("username"), rs.getString("password"), 
								rs.getString("email"), rs.getInt("role_id"), rs.getString("first_name"),rs.getString("last_name"));
				
				if(nxtUser != null)
				{
					uList.add(nxtUser);
				}
			}
			
			loggy.info("Accessed list of users.");
			return uList;
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return null;
		}
	}

	/**
	 * Selects all users in DB by the given username. <br>
	 * 
	 * @returns User
	 * @param String userN Username of user to be found.
	 * @Override
	 */
	public User selectUserbyUsername(String userN) 
	{
		User foundUser = null;
		
		loggy.info("Attempting to query database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "SELECT ru.*, ur.role_name"
					+ " FROM reimb_user ru"
					+ " LEFT JOIN user_role ur"
					+ " ON ru.role_id = ur.role_id"
					+ " WHERE ru.username = ?";
			
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userN);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				foundUser = 
						new User(rs.getInt("user_id"), rs.getString("username"), rs.getString("password"), 
						rs.getString("email"), rs.getInt("role_id"), rs.getString("first_name"),rs.getString("last_name"));

				
				loggy.info("Found user.");
				return foundUser;
			}
			
			loggy.info("No user found.");
			return foundUser; //Will be expected to return null here.
			
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return null;
		}
		
	}

	/**
	 * Selects all users in DB by the given user ID. <br>
	 * 
	 * @returns User
	 * @param int ID user id of user to be found.
	 * @Override
	 */
	public User selectUserbyUserID(int id) 
	{
		User foundUser = null;
		
		loggy.info("Attempting to query database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "SELECT ru.*, ur.role_name"
					+ " FROM reimb_user ru"
					+ " LEFT JOIN user_role ur"
					+ " ON ru.role_id = ur.role_id"
					+ " WHERE ru.user_id = ?";
			
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				foundUser = 
						new User(rs.getInt("user_id"), rs.getString("username"), rs.getString("password"), 
						rs.getString("email"), rs.getInt("role_id"), rs.getString("first_name"),rs.getString("last_name"));
				
				loggy.info("Found user.");
				return foundUser;
			}
			
			loggy.info("No user found.");
			return foundUser; //Will be expected to return null here.
			
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return null;
		}
	}

	/**
	 * insert new user. <br>
	 * 
	 * @returns User
	 * @param User user User to insert.
	 * @Override
	 */
	public boolean insertUser(User user) 
	{
		
		loggy.info("Attempting to update the database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "INSERT INTO reimb_user VALUES"
					+ " (DEFAULT, ?, ?, ?, ?, ?, ?)";
			
			
			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getFirstName());
			ps.setString(4, user.getLastName());
			ps.setString(5, user.getEmail());
			ps.setInt(6, user.getRoleId());
			
			
			
			int count = ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			
			while(rs.next())
			{
				user.setUserId(rs.getInt(1));
			}
			
			
			if(count > 0)
			{
				loggy.info(user + " Succesfully added.");
				return true;
			}
			else
			{
				loggy.info(user + " was not added.");
				return false;
			}
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}
	}

	/**
	 * Delete a user. <br>
	 * 
	 * @returns boolean true if something was deleted.
	 * @param User user User to delete.
	 * @Override
	 */
	public boolean deleteUserByID(User user) 
	{
		loggy.info("Attempting to update the database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "DELETE FROM reimb_user"
					+ " WHERE user_id = ?";
			
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getUserId());
			
			
			int count = ps.executeUpdate();
			
			if(count > 0)
			{
				loggy.info(user + " was succesfully deleted.");
				return true;
			}
			else
			{
				loggy.info(user + " was not deleted.");
				return false;
			}
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}
	}

	/**
	 * update a user. Updates every attribute of a user besides id even if nothing was changed. <br>
	 * 
	 * @returns boolean true if something was updated.
	 * @param User user User to update.
	 * @Override
	 */
	public boolean updateUserByID(User user) 
	{
		loggy.info("Attempting to update the database.");
		
		try(Connection conn = DriverManager.getConnection(url, username, password))
		{
			String sql = "UPDATE reimb_user"
					+ " SET username = ?, SET password = ?, SET first_name = ?, SET last_name = ?, SET email = ?, SET role_id = ?"
					+ " WHERE user_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getFirstName());
			ps.setString(4, user.getLastName());
			ps.setString(5, user.getEmail());
			ps.setInt(6, user.getRoleId());
			ps.setInt(7, user.getUserId());
		
			int count = ps.executeUpdate();
			
			if(count > 0)
			{
				loggy.info(user + " successfully updated.");
				return true;
			}
			else
			{
				loggy.info(user + " was not updated.");
				return false;
			}
		}
		catch(SQLException e)
		{
			loggy.error("Something went wrong when communicating with the database.", e);
			return false;
		}
	}

	/***
	 * H2 Testing create DB
	 */
	@Override
	public void h2InitDao() 
	{
		try(Connection conn = DriverManager.getConnection(url,username, password))
		{
			String sql= "CREATE TABLE reimb_user(\r\n" + 
					"	user_id SERIAL PRIMARY KEY\r\n" + 
					"	, username VARCHAR(50) NOT NULL UNIQUE\r\n" + 
					"	, password VARCHAR(50) NOT NULL\r\n" + 
					"	, first_name VARCHAR(100) NOT NULL\r\n" + 
					"	, last_name VARCHAR(100) NOT NULL\r\n" + 
					"	, email VARCHAR(150) NOT NULL UNIQUE\r\n" + 
					"	, role_id INTEGER NOT NULL);";
			
			Statement state = conn.createStatement();
			state.execute(sql);
			
		}
		catch(SQLException e) 
		{
			e.printStackTrace();
		}
		
	}

	/***
	 * H2 Testing drop DB
	 */
	@Override
	public void h2DestroyDao() 
	{
		try(Connection conn = DriverManager.getConnection(url,username, password))
		{
			String sql= "DROP TABLE reimb_user;";
			
			Statement state = conn.createStatement();
			state.execute(sql);
		}
		catch(SQLException e) 
		{
			e.printStackTrace();
		}
		
	}

}
