package models.dao;

import java.util.List;

import models.User;

public interface UsersDAO 
{
	
	public List<User> selectAllUsers();
	
	public User selectUserbyUsername(String userN);
	
	public User selectUserbyUserID(int id);
	
	public boolean insertUser(User user);
	
	public boolean deleteUserByID(User user);
	
	public boolean updateUserByID(User user);
	
	//For H2 testing
	public void h2InitDao();
	public void h2DestroyDao();
	
	
}
