package utility.helpers;

public class ReimbursementHelper 
{
	
	public int typeConversion(String typeName)
	{
		if(typeName == null)
		{
			return 4;
		}
		
		switch (typeName) 
		{
		case "Lodging":
			return 1;
			
		case "Travel":
			return 2;
			
		case "Food":
			return 3;
			
		case "Other":
			return 4;
			
		default:
			return 4;
		}
	}
	
	public int statusConversion(String statusName)
	{
		switch (statusName) 
		{
		case "Pending":
			return 1;
			
		case "Approved":
			return 2;
			
		case "Denied":
			return 3;
			
		default:
			return -1;
		}
	}

	
}
