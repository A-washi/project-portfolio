package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import models.dao.UsersDAOImpl;


public class MasterServlet extends HttpServlet
{
	final static Logger loggy = Logger.getLogger(MasterServlet.class);
	
	//Needed to use JDBC with servlets
	static {
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }
	
	
	//Note: Gets are rarely used in this application
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException 
	{
		loggy.info("Servicing a doGet.");

		req.getRequestDispatcher(RequestHelper.getProcess(req, res)).forward(req, res);

	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, JsonProcessingException
	{
		loggy.info("Servicing a doPost.");
		
		req.getRequestDispatcher(RequestHelper.postProcess(req, res)).forward(req, res);
	}
}
