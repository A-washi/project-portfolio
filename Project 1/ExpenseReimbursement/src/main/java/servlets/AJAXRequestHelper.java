package servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import controller.AJAXController;

public class AJAXRequestHelper 
{
	final static Logger loggy = Logger.getLogger(AJAXRequestHelper.class);
	
	public static String postProcess(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException 
	{
		loggy.info(req.getRequestURI());
		
		switch(req.getRequestURI()) 
		{
		
		case "/ExpenseReimbursement/AJAX/CurrentUname" :
			AJAXController.sendUsername(req, res);
			break;
			
		case "/ExpenseReimbursement/AJAX/UserInformation" :
			AJAXController.getUserReimbursements(req, res);
			break;
			
		case "/ExpenseReimbursement/AJAX/submitReimbursement" :
			AJAXController.submitEmployeeReimbursement(req, res);
			break;
			
		case "/ExpenseReimbursement/AJAX/approveReimbursement" :
			AJAXController.approveReimbursement(req, res);
			break;
			
		case "/ExpenseReimbursement/AJAX/denyReimbursement" :
			AJAXController.denyReimbursement(req, res);
			break;

		}
		
		return null;
	}
	
}
