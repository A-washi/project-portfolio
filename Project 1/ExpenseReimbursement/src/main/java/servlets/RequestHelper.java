package servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import controller.LoginController;


public class RequestHelper 
{
	final static Logger loggy = Logger.getLogger(RequestHelper.class);
	
	//Having separate helper methods is not necessary, but was for my own understanding.
	public static String postProcess(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException 
	{
		loggy.info("POSTint "+ req.getRequestURI() );
		System.out.println(req.getRequestURI());
		
		switch(req.getRequestURI()) 
		{
		case "/ExpenseReimbursement/login":
			
			return LoginController.login(req, res);
			
		case "/ExpenseReimbursement/login/incorrectcredentials":
			
			return LoginController.login(req, res);
			
		case "/ExpenseReimbursement/reimbursements/reimbursement-employee":
			return "/html/reimbursement-employee.html";
			
		case "/ExpenseReimbursement/reimbursements/reimbursement-submission":
			return "/html/reimbursement-submission.html";
			
			
		default:
			System.out.println("issue!!!");
			return "/html/login.html";	
				
				
				//response.getWriter().write(new ObjectMapper().writeValueAsString(badAsk));
		}
	}
	
	public static String getProcess(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException 
	{
		loggy.info("GETtint "+ req.getRequestURI() );
		
		switch(req.getRequestURI()) 
		{
			
		case "/ExpenseReimbursement/login":
			return "/html/reimbursement-employee.html";
			
			
		case "/ExpenseReimbursement/reimbursements/reimbursement-submission":
			return "/html/reimbursement-submission.html";

			
		default:
			System.out.println("issue!!!");
			return "/resources/html/login.html";	
				
				
		}
	}


}
