package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

public class AjaxServlet extends HttpServlet
{
	final static Logger loggy = Logger.getLogger(AjaxServlet.class);
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, JsonProcessingException
	{
		loggy.info("Servicing AJAX request.");
		System.out.println("in the do post - AJAX.");
		
		AJAXRequestHelper.postProcess(req, res);
	}

}
