DROP TABLE reimbursement;
DROP TABLE reimb_user;
DROP TABLE reimbursement_type;
DROP TABLE user_role;
DROP TABLE reimbursement_status;



CREATE TABLE reimbursement_status(
	status_id SMALLSERIAL PRIMARY KEY
	, status_name VARCHAR(10) NOT NULL
	
);

INSERT INTO reimbursement_status (status_name) VALUES
	('Pending'), ('Approved'), ('Denied');
	

CREATE TABLE reimbursement_type(
	type_id SMALLSERIAL PRIMARY KEY
	, type_name VARCHAR(10) NOT NULL

);

INSERT INTO reimbursement_type (type_name) VALUES
	('Lodging'), ('Travel'), ('Food'), ('Other');


CREATE TABLE user_role(
	role_id SMALLSERIAL PRIMARY KEY
	, role_name VARCHAR(10) NOT NULL
);

INSERT INTO user_role (role_name)
	VALUES ('Employee'), ('FinManager'/*Finance Manager*/);

CREATE TABLE reimb_user(
	user_id SERIAL PRIMARY KEY
	, username VARCHAR(50) NOT NULL UNIQUE
	, password VARCHAR(50) NOT NULL
	, first_name VARCHAR(100) NOT NULL
	, last_name VARCHAR(100) NOT NULL
	, email VARCHAR(150) NOT NULL UNIQUE
	, role_id INTEGER NOT NULL)
	
	, CONSTRAINT fk_user_role FOREIGN KEY (role_id) REFERENCES user_role (role_id)
		ON DELETE SET NULL

);

CREATE TABLE reimbursement(
	reimb_id SERIAL PRIMARY KEY
	, amount DECIMAL(30,2) NOT NULL
	, submitted TIMESTAMP NOT NULL DEFAULT NOW()
	, resolved TIMESTAMP 
	, description VARCHAR(250)
	, receipt BYTEA NULL
	, author INTEGER NOT NULL
	, resolver INTEGER NULL
	, status_id INTEGER NULL
	, type_id INTEGER NULL 
	
	, CONSTRAINT fk_author FOREIGN KEY (author) REFERENCES reimb_user (user_id)
		ON DELETE NO ACTION
	, CONSTRAINT fk_resolver FOREIGN KEY (resolver) REFERENCES reimb_user (user_id)
		ON DELETE NO ACTION
	, CONSTRAINT fk_status_id FOREIGN KEY (status_id) REFERENCES reimbursement_status (status_id)
		ON DELETE SET NULL
	, CONSTRAINT fk_type_id FOREIGN KEY (type_id) REFERENCES reimbursement_type (type_id)
		ON DELETE SET NULL
);

INSERT INTO reimb_user VALUES 
	(DEFAULT, 'Rix-Rick32', 'RIXXY34$', 'Gerald', 'Marvins', 'G.marv@hotmail.com', 1),
	(DEFAULT, 'Arrow3', 'Str1f3', 'Carl', 'Lutz', 'CloudXAerith44532@Aol.com', 2),
	(DEFAULT, 'BColl', 'Crux1s', 'Colette', 'Brunel', 'BrunelC_00@gmail.com', 1),
	(DEFAULT, 'test', 'test', 'Lester', 'Testington', 'tester@hotmail.com', 1),
	(DEFAULT, 'testMan', 'test', 'Chester', 'Testinghouse', 'tester@yahoo.com', 2);

INSERT INTO reimbursement (amount, submitted, description, author, status_id, type_id)
	VALUES (203.45, NOW(), 'For stuffs!', 1, 3, 1);

INSERT INTO reimbursement (amount, submitted, description, author, status_id, type_id)
	VALUES (2345.45, NOW(), 'For things!', 4, 1, 3)
	, (2.43, NOW(), 'Boss gift', 4, 1, 3)
	, (940.00, NOW(), 'For more things!', 4, 1, 3);

